# MultiresolutionAlgorithm

## Installation
For distributed version, a MPI cluster configured is required.
All python libraries required are available in environment.yml file.
Once Cellpose installed, move ./data/model/CP_20220420_140301 file to ~/.cellpose/models

## Run the code

Modify model_type path in decision_block function, line 30.
When located in src directory, multiresolution algorithm can be run using following command:
mpirun -n x -hostfile hosts /path/to/env/bin/python -m mpi4py multiresolution_algo_dist.py
