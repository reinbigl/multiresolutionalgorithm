from PIL import Image,ImageDraw, ImageShow
import numpy as np
#import matplotlib.pyplot as plt
import time, os, sys
from cellpose import utils, io, models
import tifffile
import networkx as nx
from mpi4py import MPI
import random

def decision_block(image, rank, position_x, position_y):
    """Decide wether to zoom in or not if the size of segmented mask is smaller than a thereshold. Returns the number of masks in the input image.

    Args:
        image (_type_): image to be segmented
        rank (_type_): rank of the process to make the analysis

    Returns:
        _type_: Number of segmentation masks
    """
    name = "S35"
    #print(f"Cellpose will start for image {name}")
    image.save("/var/tmp/reinbigl/results/S35_" + str(rank) + ".tiff")
    image = io.imread("/var/tmp/reinbigl/results/S35_" + str(rank) + ".tiff")
    '''
    model = models.CellposeModel(gpu=False, model_type='/netfs/ei1821/reinbigl/.cellpose/models/CP_20220420_140301')

    channels = [[3,0]]

    t1=time.time()

    masks, flows, styles = model.eval(image, diameter=50, flow_threshold=0.5, resample=True, channels=channels)    

    t2 =  time.time()
    '''
    #print(f"Segmentation done in {t2-t1} seconds")

    # save results as png

    #io.save_masks(image, masks, flows, "out_filename", savedir="/var/tmp/reinbigl/results/")

    #t3 =  time.time()

    #outlines = utils.outlines_list(masks)
    #io.outlines_to_text(out_filename, outlines)

    #t3 =  time.time()
    '''
    times = open(out_path + "/" + times_filename, "a")
    times.write("{};{};{};{};\n".format(out_filename, t2-t1, t3-t2, t3-t1))
    times.close()
    '''
    #random.seed(position_x*position_y)
    #sleep_time = random.randint(1,10)
    sleep_time = 5
    print(f"I'll sleep {sleep_time} seconds")
    time.sleep(sleep_time)  

    '''
    if np.all(masks==0):
        return 0, False
    else:
        count = len(np.unique(masks)) -1
        #print("count: " + str(count))
        eps = 500
        unique, counts = np.unique(masks, return_counts=True)
        if min(counts) < eps:
            return count, True
        else :
            return count, False
    '''
    mu, sigma = 0.5, 0.5
    np.random.seed(position_x*position_y)
    s = np.random.normal(mu, sigma, 1)
    if (s > 0.5):
        return s, True
    else:
        return s, False



def go_through_resolution_V2(image_path, image_name, scale_factor, res_level, max_res_level, tile, tile_width, tile_height, position_x, position_y, global_count, rank, comm):
    """Recursive function applying the analysis and going through resolution in patches where required. 

    Args:
        image_path (_type_): path to image to be analized
        image_name (_type_): name of image to be analyzed
        scale_factor (_type_): The factor between two resolution level
        res_level (_type_): The level of resolution
        max_res_level (_type_): The maximum resolution level available
        tile (_type_): The tile on which to apply the analysis
        tile_width (_type_): The width of the tile
        tile_height (_type_): The height of a tile
        position_x (_type_): Location in x of the tile
        position_y (_type_): Location in y of the tile
        global_count (_type_): The sum of number of patch for the last resolution level
        rank (_type_): rank of process performing the analysis
    """

    count, zoom_in = decision_block(tile, rank, position_x, position_y)



    if zoom_in: 
        #print("count zoom in:" + str(count))
        if scale_factor*res_level <= max_res_level:

            print("Scale_factor: " + str(scale_factor*res_level))
            mm = tifffile.memmap(image_path+ "/" + str(res_level*scale_factor) + "/" + image_name)
            new_position_x = position_x*scale_factor
            new_position_y = position_y*scale_factor
            for i in range(scale_factor):
                for j in range(scale_factor):
                    x = new_position_x+i*tile_width
                    y = new_position_y+j*tile_height
                    data = {"res_level": res_level*scale_factor, "x": x, "y": y}
                    req = comm.isend(data, dest = 0, tag = 22)
                    req.wait()
                    
                    #tile = Image.fromarray(mm[new_position_x+i*tile_width:new_position_x+(i+1)*tile_width, new_position_y+j*tile_height:new_position_y+(j+1)*tile_height])         
                    #global_count = go_through_resolution_V2(image_path, image_name, scale_factor, res_level*scale_factor, max_res_level, tile, tile.size[0], tile.size[1], new_position_x+i*tile_width, new_position_y+j*tile_height, global_count, rank) 
            log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_min_makespan_multi_gen_profile_worker_" + str(rank), 'a')
            log_file.write(f"{res_level}({position_x},{position_x+tile_width},{position_y},{position_y+tile_height});")
            log_file.close()
            data = {"reso_level": res_level, "x": position_x, "y": position_y , "count": count}
            req = comm.isend(data, dest = 0, tag = 33)
            return #global_count

        else:
            global_count= count 
            print("global_count " + str(global_count))
            log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_min_makespan_multi_gen_profile_worker_" + str(rank), 'a')
            log_file.write(f"{res_level}({position_x},{position_x+tile_width},{position_y},{position_y+tile_height});")
            log_file.close()
            data = {"reso_level": res_level, "x": position_x, "y": position_y , "count": count}
            req = comm.isend(data, dest = 0, tag = 33)
            req.wait()
            return #global_count    
    else:
        global_count= count 
        log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_min_makespan_multi_gen_profile_worker_" + str(rank), 'a')
        log_file.write(f"{res_level}({position_x},{position_x+tile_width},{position_y},{position_y+tile_height});")
        log_file.close()
        print("global_count " + str(global_count))
        data = {"reso_level": res_level, "x": position_x, "y": position_y , "count": count}
        req = comm.isend(data, dest = 0, tag = 33)
        req.wait()
        return  #global_count


def build_graph_V2(image_path, image_name, scale_factor, max_res_level, tile_width, tile_height, func, rank, size, comm):
    """Distributes data among workers before laucnhing the analysis. It gathers the results and add it to the final grapĥ.

    Args:
        image_path (_type_): path to image to be analyzed/
        image_name (_type_): name of image to be analyzed
        scale_factor (_type_): factor btween two resolution level
        max_res_level (_type_): maximum resolution level accessible
        tile_width (_type_): width of a tile
        tile_height (_type_): height of a tile
        func (_type_): analysis block to be applied
        rank (_type_): MPI process rank
        size (_type_): size of MPI pool of processes
        comm (_type_): MPI communicator
    """
    global_count=0
    mm = tifffile.memmap(image_path+ "/1/" + image_name)
    print(mm.shape[0])
    print(mm.shape[1])    
    
    if rank == 0:
        G = nx.grid_graph(((int(mm.shape[1]/tile_width)+2), int(mm.shape[0]/tile_height)+2), periodic=False)
        G.nodes(data="count", default=0)

    size_portion = int((int(mm.shape[1]/tile_width)+1) * (int(mm.shape[0]/tile_height)+1) / size)
    tile_per_line = (int(mm.shape[1]/tile_width)+1)
    tile_per_column = int(mm.shape[0]/tile_height)+1


    if rank == 0:

        messages_in_transit = 0
        data = [(1, i) for i in range((int(mm.shape[1]/tile_width)+1) * (int(mm.shape[0]/tile_height)+1))]
        for i in range(1,size):
            k=data.pop(0)
            req = comm.isend(k, dest=i, tag=11)
            messages_in_transit+=1
            req.wait()
            print(f'sent {k} to {i}')

        dest_rank = 0
        while ((len(data)>0) or (messages_in_transit >0)):
            #print(f"len_data: {len(data)}")
            status = MPI.Status()
            res = comm.iprobe(source=MPI.ANY_SOURCE, tag = 11, status = status)
            if res:
                data_recv = comm.recv(source=MPI.ANY_SOURCE, tag=11)
                messages_in_transit-=1
                print(f"message_in_transit: {messages_in_transit}")
                destination = status.Get_source() 
                print(f'receives message from {destination}')
                #G.nodes[data_recv['i'],data_recv['j']]["count"] = data_recv['count']
                #print("Global : " + str(G.nodes[data_recv['i'],data_recv['j']]["count"]))


                if (len(data)>0) :
                    k = data.pop(0)
                    req = comm.isend(k, dest=destination, tag=11)
                    messages_in_transit+=1
                    print(f"message_in_transit: {messages_in_transit}")
                    req.wait()
                    print(f'sent: {k} to {destination}')
                else :
                    print("message in transit: " + str(messages_in_transit))
                    if (messages_in_transit == 0):
                        for i in range(1,size):
                            req = comm.isend(" ", dest=i, tag=1)
                            req.wait()
                            print(f'sent termination to {i}')

            res = comm.iprobe(source=MPI.ANY_SOURCE, tag = 22, status = status)
            if res:
                data_recv = comm.recv(source=MPI.ANY_SOURCE, tag=22)
                #messages_in_transit-=1
                print(f"message_in_transit: {messages_in_transit}")
                x = data_recv['x']
                y = data_recv['y']
                reso_level = data_recv['res_level']
                mm_res = tifffile.memmap(image_path+ "/" + str(reso_level) + "/" + image_name)
                tile_per_line_res = (int(mm_res.shape[1]/tile_width)+1)
                tile_id = y//tile_height * tile_per_line_res + x//tile_width
                data.append((reso_level, tile_id))
            
            res = comm.iprobe(source=MPI.ANY_SOURCE, tag = 33, status = status)
            if res:
                data_recv = comm.recv(source=MPI.ANY_SOURCE, tag=33)
                #messages_in_transit-=1
                destination = status.Get_source() 
                x = data_recv['x']
                y = data_recv['y']
                reso_level = data_recv['reso_level']
                count = data_recv['count']
                
                j = y//(reso_level*tile_height)
                i = x//(reso_level*tile_width)  

                print(i,j, reso_level)
                if "count" in G.nodes[i,j]:
                    G.nodes[i,j]["count"] += count
                else :
                    G.nodes[i,j]["count"] = count
                
                
                '''
                if "count" in G.nodes[i,j]:
                    G.nodes[i,j]["count"].append((reso_level,count))
                else :
                    G.nodes[i,j]["count"] = [(reso_level,count)] 
                '''

                    
            #print("Moving to the next tile")




    elif rank !=0:
        while True:
            status = MPI.Status()
            res = comm.iprobe(source=0, tag = MPI.ANY_TAG, status = status)
            if res:
                print(status.Get_source())
                msg = comm.recv(source=0)
                print("I received: " + str(msg))
                if status.Get_tag() == 1:
                    print('terminating')
                    break
                reso_level =  msg[0]
                mm = tifffile.memmap(image_path+ "/" + str(reso_level) + "/" + image_name)
                #print(mm.shape)
                tile_per_line = (int(mm.shape[1]/tile_width)+1)
                tile_per_column = int(mm.shape[0]/tile_height)+1
                i, j = msg[1]%(tile_per_line), msg[1]//(tile_per_line)
                #print(i,j, reso_level)

                
                x_max = min((i+1)*tile_width, mm.shape[1])
                y_max = min((j+1)*tile_height, mm.shape[0])
                #print("x_max:" + str(x_max) + " for rank " + str(rank))
                #print("y_max:" + str(y_max) + " for rank " + str(rank))
                #print(j*tile_height,y_max, i*tile_width,x_max)
                tile = mm[j*tile_height:y_max, i*tile_width:x_max]             
                tile = Image.fromarray(tile)
                global_count = 0
                t1 = time.time()
                go_through_resolution_V2(image_path, image_name, scale_factor, reso_level, max_res_level, tile, tile.size[0], tile.size[1], i*tile_width , j*tile_height, global_count, rank, comm)
                #print(global_count)
                t2=time.time()
                print(f"Execution time: {t2-t1} for tile positioned at ({i*tile_width},{x_max},{j*tile_height},{y_max})\n")
                log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_min_makespan_multi_gen_profile_worker_" + str(rank), 'a')
                log_file.write(f"{t2-t1}; ({i*tile_width},{x_max},{j*tile_height},{y_max})\n")
                log_file.close()
            
                #data = {'i': i, 'j': j, 'count': global_count}
                
            
            
            
               
                req  = comm.isend(None, dest=0, tag=11)
                req.wait()
                print(req)
                print(f'{rank} sent: msg to 0')      


    
    if rank == 0:
        return G
    else :
        return


def main():

    #pr = cProfile.Profile()
    #pr.enable()
    #mpi4py.profile('mpe', logfile='mpi4py')

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = MPI.COMM_WORLD.Get_size();
    
    tile_width = 256
    tile_height = 256
    image_path = "/var/tmp/reinbigl/multiresolutionalgorithm/data/Test_image/"
    image_name = "S35L1-Scene-18-QUADc3.tif" #S42L1-Scene-17-QUADc2.tif

    if os.path.exists("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_min_makespan_multi_gen_profile_worker_" + str(rank)):
        os.remove("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_min_makespan_multi_gen_profile_worker_" + str(rank))

    if rank == 0:
        print("I'm the root")
        rank=0
    
    else:
        print("My rank is : " + str(rank))
    
        print("My rank is : " + str(rank) + " and I'm launched on " + MPI.Get_processor_name())
    
    t1 = time.time()
    
    if rank == 0:
        G = build_graph_V2(image_path, image_name, 2, 8, tile_width, tile_height, decision_block, rank, size, comm)
        print(list(G.nodes(data="count")))
    else :
        G = build_graph_V2(image_path, image_name, 2, 8, tile_width, tile_height, decision_block, rank, size, comm)
    
    t2 = time.time()
    log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_min_makespan_multi_gen_profile_worker_" + str(rank), 'a')
    log_file.write(f"Total execution time per worker: {t2-t1}")
    log_file.close()



    #print("I'm finished: " + str(rank))
    '''
    pr.disable()
    pr.dump_stats('cpu_%d.prof' %comm.rank)
    with open( 'cpu_%d.txt' %comm.rank, 'w') as output_file:
        sys.stdout = output_file
        pr.print_stats( sort='time' )
    sys.stdout = sys.__stdout__
    '''
    MPI.Finalize()


if __name__ == "__main__":
    main()
