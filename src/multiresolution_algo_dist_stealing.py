#!/usr/bin/env python

from PIL import Image,ImageDraw, ImageShow
import numpy as np
#import matplotlib.pyplot as plt
import time, os, sys
from cellpose import utils, io, models
import tifffile
import networkx as nx
from mpi4py import MPI
import random
#import socket
#import os


def decision_block(image, rank):
    """Decide wether to zoom in or not if the size of segmented mask is smaller than a thereshold. Returns the number of masks in the input image.

    Args:
        image (_type_): image to be segmented
        rank (_type_): rank of the process to make the analysis

    Returns:
        _type_: Number of segmentation masks
    """
    name = "S35"
    #print(f"Cellpose will start for image {name}")
    image.save("/var/tmp/reinbigl/results/S35_" + str(rank) + ".tiff")
    image = io.imread("/var/tmp/reinbigl/results/S35_" + str(rank) + ".tiff")
    model = models.CellposeModel(gpu=False, model_type='/netfs/ei1821/reinbigl/.cellpose/models/CP_20220420_140301')

    channels = [[3,0]]

    t1=time.time()

    masks, flows, styles = model.eval(image, diameter=50, flow_threshold=0.5, resample=True, channels=channels)    

    t2 =  time.time()

    #print(f"Segmentation done in {t2-t1} seconds")

    # save results as png

    #io.save_masks(image, masks, flows, "out_filename", savedir="/var/tmp/reinbigl/results/")

    #t3 =  time.time()

    #outlines = utils.outlines_list(masks)
    #io.outlines_to_text(out_filename, outlines)

    #t3 =  time.time()
    '''
    times = open(out_path + "/" + times_filename, "a")
    times.write("{};{};{};{};\n".format(out_filename, t2-t1, t3-t2, t3-t1))
    times.close()
    '''
    if np.all(masks==0):
        return 0, False
    else:
        count = len(np.unique(masks)) -1
        print("count: " + str(count))
        eps = 500
        unique, counts = np.unique(masks, return_counts=True)
        if min(counts) < eps:
            return count, True
        else :
            return count, False


def go_through_resolution_V2(image_path, image_name, scale_factor, res_level, max_res_level, tile, tile_width, tile_height, position_x, position_y, global_count, rank):
    """Recursive function applying the analysis and going through resolution in patches where required. 

    Args:
        image_path (_type_): path to image to be analized
        image_name (_type_): name of image to be analyzed
        scale_factor (_type_): The factor between two resolution level
        res_level (_type_): The level of resolution
        max_res_level (_type_): The maximum resolution level available
        tile (_type_): The tile on which to apply the analysis
        tile_width (_type_): The width of the tile
        tile_height (_type_): The height of a tile
        position_x (_type_): Location in x of the tile
        position_y (_type_): Location in y of the tile
        global_count (_type_): The sum of number of patch for the last resolution level
        rank (_type_): rank of process performing the analysis
    """
    count, zoom_in = decision_block(tile, rank)
    if zoom_in: 
        #print("count zoom in:" + str(count))
        if scale_factor*res_level <= max_res_level:

            print("Scale_factor: " + str(scale_factor*res_level))
            mm = tifffile.memmap(image_path+ "/" + str(res_level*scale_factor) + "/" + image_name)
            new_position_x = position_x*scale_factor
            new_position_y = position_y*scale_factor
            for i in range(scale_factor):
                for j in range(scale_factor):
                    tile = Image.fromarray(mm[new_position_x+i*tile_width:new_position_x+(i+1)*tile_width, new_position_y+j*tile_height:new_position_y+(j+1)*tile_height])         
                    global_count = go_through_resolution_V2(image_path, image_name, scale_factor, res_level*scale_factor, max_res_level, tile, tile.size[0], tile.size[1], new_position_x+i*tile_width, new_position_y+j*tile_height, global_count, rank) 
            log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_rand_profile_worker_" + str(rank), 'a')
            log_file.write(f"{res_level}({position_x},{position_x+tile_width},{position_y},{position_y+tile_height});")
            log_file.close()
            return global_count

        else:
            global_count+= count 
            print("global_count " + str(global_count))
            log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_rand_profile_worker_" + str(rank), 'a')
            log_file.write(f"{res_level}({position_x},{position_x+tile_width},{position_y},{position_y+tile_height});")
            log_file.close()
            return global_count    
    else:
        global_count+= count 
        log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_rand_profile_worker_" + str(rank), 'a')
        log_file.write(f"{res_level}({position_x},{position_x+tile_width},{position_y},{position_y+tile_height});")
        log_file.close()
        print("global_count " + str(global_count))
        return  global_count


def build_graph_V2(image_path, image_name, scale_factor, max_res_level, tile_width, tile_height, func, rank, size, comm):
    """Distributes data among workers before laucnhing the analysis. It gathers the results and add it to the final grapĥ.

    Args:
        image_path (_type_): path to image to be analyzed/
        image_name (_type_): name of image to be analyzed
        scale_factor (_type_): factor btween two resolution level
        max_res_level (_type_): maximum resolution level accessible
        tile_width (_type_): width of a tile
        tile_height (_type_): height of a tile
        func (_type_): analysis block to be applied
        rank (_type_): MPI process rank
        size (_type_): size of MPI pool of processes
        comm (_type_): MPI communicator
    """
    global_count=0
    
    mm = tifffile.memmap(image_path+ "/1/" + image_name)
    if rank == 0:
        G = nx.grid_graph(((int(mm.shape[1]/tile_width)+2), int(mm.shape[0]/tile_height)+2), periodic=False)
    print(mm.shape[0])
    print(mm.shape[1])
    size_portion = int((int(mm.shape[1]/tile_width)+1) * (int(mm.shape[0]/tile_height)+1) / size)
    tile_per_line = (int(mm.shape[1]/tile_width)+1)
    tile_per_column = int(mm.shape[0]/tile_height)+1
    if rank==0:
        workqueue = [i for i in range((int(mm.shape[1]/tile_width)+1) * (int(mm.shape[0]/tile_height)+1))]
        random.shuffle(workqueue)
        for i in range(1,size):
            
            if (i == (size-1)):
                data = np.array(workqueue[i*size_portion: (int(mm.shape[1]/tile_width)+1) * (int(mm.shape[0]/tile_height)+1)], dtype='i')
                print(data.size)
            else :
                data = np.array(workqueue[i*size_portion:(i+1)*size_portion], dtype='i')
            
            comm.Send([data, MPI.INT], dest=i)
        workqueue = workqueue[:size_portion]
    
    if rank != 0:
        if (rank == (size-1)):
            data = np.empty((int(mm.shape[1]/tile_width)+1) * (int(mm.shape[0]/tile_height)+1)-rank*size_portion, dtype='i')
        else:
            data = np.empty(size_portion, dtype='i')
        comm.Recv([data, MPI.INT], source=0)

        workqueue = data.tolist()

    workqueue = [(1,elt) for elt in workqueue]



    while work


    for l in range(len(workqueue)):
        print("workqueue(l): " + str(workqueue[l]))
        i, j = workqueue[l]%tile_per_line, workqueue[l]//tile_per_line
        print(i,j)
         
        x_max = min((i+1)*tile_width, mm.shape[1])
        y_max = min((j+1)*tile_height, mm.shape[0])
        print("x_max:" + str(x_max) + " for rank " + str(rank))
        print("y_max:" + str(y_max) + " for rank " + str(rank))

        tile = mm[j*tile_height:y_max, i*tile_width:x_max]             
        tile = Image.fromarray(tile)
        global_count = 0
        t1 = time.time()
        global_count = go_through_resolution_V2(image_path, image_name, scale_factor, 1, max_res_level, tile, tile.size[0], tile.size[1], i*tile_width , j*tile_height, global_count, rank)
        print(global_count)
        t2=time.time()
        print(f"Execution time: {t2-t1} for tile positioned at ({i*tile_width},{x_max},{j*tile_height},{y_max})\n")
        log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_rand_profile_worker_" + str(rank), 'a')
        log_file.write(f"{t2-t1}; ({i*tile_width},{x_max},{j*tile_height},{y_max})\n")
        log_file.close()
        data_recv=[]
        data = {'i': i, 'j': j, 'count': global_count}
        if rank != 0:
            comm.send(data, dest=0)

        if rank==0:
            G.nodes[i,j]["count"] = global_count
            for r in range(1,size):
                data_recv.append(comm.recv(source=r))
            
            #print("I'm the root")
            for s in range(len(data_recv)):
                G.nodes[data_recv[s]['i'],data_recv[s]['j']]["count"] = data_recv[s]['count']
                print("Global : " + str(G.nodes[data_recv[s]['i'],data_recv[s]['j']]["count"]))
            
        print("Moving to the next tile")
    
    if rank == 0:
        return G
    else :
        return



def main():
    #pr = cProfile.Profile()
    #pr.enable()
    #mpi4py.profile('mpe', logfile='mpi4py')

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = MPI.COMM_WORLD.Get_size();
    
    tile_width = 256
    tile_height = 256
    image_path = "/var/tmp/reinbigl/multiresolutionalgorithm/data/Test_image/"
    image_name = "S35L1-Scene-18-QUADc3.tif" #S42L1-Scene-17-QUADc2.tif

    if os.path.exists("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_rand_profile_worker_" + str(rank)):
        os.remove("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_rand_profile_worker_" + str(rank))

    if rank == 0:
        print("I'm the root")
        rank=0
    
    else:
        print("My rank is : " + str(rank))
    
        print("My rank is : " + str(rank) + " and I'm launched on " + MPI.Get_processor_name())
    
    t1 = time.time()
    
    if rank == 0:
        G = build_graph_V2(image_path, image_name, 2, 8, tile_width, tile_height, decision_block, rank, size, comm)
        print(list(G.nodes(data="count")))
    else :
        G = build_graph_V2(image_path, image_name, 2, 8, tile_width, tile_height, decision_block, rank, size, comm)
    
    t2 = time.time()
    log_file = open("/var/tmp/reinbigl/results/" + image_name.split('.')[0] + "_rand_profile_worker_" + str(rank), 'a')
    log_file.write(f"Total execution time per worker: {t2-t1}")
    log_file.close()



    #print("I'm finished: " + str(rank))
    '''
    pr.disable()
    pr.dump_stats('cpu_%d.prof' %comm.rank)
    with open( 'cpu_%d.txt' %comm.rank, 'w') as output_file:
        sys.stdout = output_file
        pr.print_stats( sort='time' )
    sys.stdout = sys.__stdout__
    '''
    MPI.Finalize()




if __name__ == "__main__":
    main()
